﻿using Postnauka.Pages;

namespace Postnauka.Steps
{
    class ProfileSteps
    {
        private MainPage _mainPage = new MainPage();
        private ProfilePage _profilePage = new ProfilePage();
        private AuthorizationSteps _authorization = new AuthorizationSteps();

        public bool CheckHistoryOption(string email, string password)
        {
            _authorization.SuccessfulAuthorization(email, password);
            string articleName = _mainPage.ChooseTheBiggestCard();
            _mainPage.ClickLoginedButton();
            _profilePage.ClickHistoryButton();
            return _profilePage.IsLastArticleNameEqual(articleName);
        }
    }
}
