﻿using Postnauka.Pages;

namespace Postnauka.Steps
{
    class ThemesGamesCoursesSteps
    {
        private MainPage _mainPage = new MainPage();
        private CardsDesktopPage _cardsDesktop = new CardsDesktopPage();
        private ArticlePage _articlePage = new ArticlePage();
        private CoursesPage _coursesPage = new CoursesPage();
        private GameTestPage _gameTest = new GameTestPage();
        private AllThemesPage _themesPage = new AllThemesPage();

        public bool CompareArticleTheme()
        {
            _mainPage.MoveYoThemesButton();
            string theme = _mainPage.ClickRandomTheme();
            _cardsDesktop.ClickCard();
            return _articlePage.IsThemeEqual(theme);
        }

        public bool CompareFirstLetters()
        {
            _mainPage.MoveYoThemesButton();
            _mainPage.ClickAllThemesLink();
            return _themesPage.CheckFirstLetter(_themesPage.ClickAlphabetLetter());
        }

        public bool CompareCourseMaterialsNumber()
        {
            _mainPage.ClickCoursesButton();
            string numberOfCourseMaterials = _cardsDesktop.GetNumberOfCourseMaterials();
            _cardsDesktop.ClickCard();
            return _coursesPage.GetNumberOfCourseMaterials() == numberOfCourseMaterials;
        }

        public bool CompareGameQuestionsNumber()
        {
            _mainPage.ClickGamesButton();
            _cardsDesktop.ClickCard();
            _gameTest.ClickStartTestButton();
            return _gameTest.GetNumberOfQuestions() == _gameTest.ClickAnswerOptions();
        }
    }
}
