﻿using Postnauka.Pages;

namespace Postnauka.Steps
{
    class AuthorizationSteps
    {
        private MainPage _mainPage = new MainPage();
        private AuthorizationPage _authPage = new AuthorizationPage();

        public bool SuccessfulAuthorization(string email, string password)
        {
            AuthorizationFormInput(email, password);
            return _authPage.IsAuthorizationSuccessful();
        }

        public bool IncorrectEmail(string email, string password)
        {
            AuthorizationFormInput(email, password);
            return _authPage.IsEmailErrorMessageAppeared();
        }

        public bool IncorrectPassword(string email, string password)
        {
            AuthorizationFormInput(email, password);
            return _authPage.IsPasswordErrorMessageAppeared();
        }

        private void AuthorizationFormInput(string email, string password)
        {
            _mainPage.ClickLogInButton();
            _authPage.EnterEmail(email);
            _authPage.EnterPassword(password);
            _authPage.ClickSubmitButton();
        }
    }
}
