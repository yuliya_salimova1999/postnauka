﻿using Postnauka.Pages;

namespace Postnauka.Steps
{
    class SearchAnimateRandomizeSteps
    {
        private MainPage _mainPage = new MainPage();
        private ArticlePage _articlePage = new ArticlePage();

        public bool Search(string input)
        {
            _mainPage.ClickSearchButton();
            _mainPage.EnterSearchFieldInput(input);
            _mainPage.ClickSearchSubmitButton();
            return _mainPage.IsSearchWorks();
        }

        public bool Animate()
        {
            _mainPage.ClickAnimateButton();
            return _mainPage.IsAnimatedButtonWorks();
        }

        public bool Randomize()
        {
            _mainPage.ClickRandomizerButton();
            string firstArticleName = _articlePage.GetArticleName();
            _mainPage.ClickRandomizerButton();
            return _articlePage.IsHeaderNotEqual(firstArticleName);
        }
    }
}
