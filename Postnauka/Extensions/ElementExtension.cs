﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Threading;

namespace Postnauka.Extensions
{
    class ElementExtension
    {
        private By _locator;
        private WebDriverWait _wait;
        private IWebElement _webElement => _wait.Until(driver => WebDriver.GetWebDriver(_currentThreadID).FindElement(_locator));
        private int _currentThreadID = Thread.CurrentThread.ManagedThreadId;
        private Interaction _interaction;

        public ElementExtension(By locator, int timeout = 20)
        {
            _locator = locator;
            _wait = new WebDriverWait(WebDriver.GetWebDriver(_currentThreadID), TimeSpan.FromSeconds(timeout));
            _wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
        }

        public void Click()
        {
            _interaction = ClickThis;
            _wait.Until(driver => IsInteractible(_interaction) == true);
        }

        public ReadOnlyCollection<IWebElement> GetCollection() => _wait.Until(driver => driver.FindElements(_locator));

        public void SendKeys(string input)
        {
            _interaction = SendKeysThis;
            _wait.Until(driver => IsInteractible(_interaction, input));
        }

        public void MoveTo() => new Actions(WebDriver.GetWebDriver(_currentThreadID)).MoveToElement(_webElement).Build().Perform();

        public bool IsNotEqualWait(string text)
        {
            _wait.Until(driver => GetText().Equals(text) == false);
            return true;
        }

        public string GetText() => _wait.Until(driver => _webElement.Text);

        public IWebElement FindElement(By locator)
        {
            return _wait.Until(driver => _webElement.FindElement(locator));
        }

        public bool IsExists()
        {
            try
            {
                return _wait.Until(driver => GetCollection().Count > 0);
            }
            catch
            {
                return false;
            }
        }

        private delegate void Interaction(string input);

        private void ClickThis(string empty) => _webElement.Click();

        private void SendKeysThis(string input) => _webElement.SendKeys(input);

        private bool IsInteractible(Interaction interaction, string input = "")
        {
            try
            {
                interaction(input);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}