﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Concurrent;

namespace Postnauka.Extensions
{
    public static class WebDriver
    {
        private static ConcurrentDictionary<int, IWebDriver> _dictionary = new ConcurrentDictionary<int, IWebDriver>();

        public static IWebDriver GetWebDriver(int threadID)
        {
            if (_dictionary.TryGetValue(threadID, out var driver))
            {
                return driver;
            }
            return CreateDriver(threadID);
        }

        public static void RemoveDriver(int threadID)
        {
            _dictionary.TryRemove(threadID, out var driver);
        }

        private static IWebDriver CreateDriver(int threadID)
        {
            IWebDriver driver = new ChromeDriver();
            _dictionary.TryAdd(threadID, driver);
            return driver;
        }
    }
}
