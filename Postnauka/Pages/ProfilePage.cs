﻿using OpenQA.Selenium;
using Postnauka.Extensions;

namespace Postnauka.Pages
{
    class ProfilePage
    {
        private ElementExtension _historyButton = new ElementExtension(By.XPath("//a[contains(@href,'views' )]"));
        private ElementExtension _firstCardTitle = new ElementExtension(By.XPath("//div[contains(@class,'cards-grid')]/a[1]//div[@class='card-desktop__standart-title']/span"));

        public void ClickHistoryButton() => _historyButton.Click();

        public bool IsLastArticleNameEqual(string text) => _firstCardTitle.GetText() == text;
    }
}
