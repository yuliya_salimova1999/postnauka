﻿using OpenQA.Selenium;
using Postnauka.Extensions;

namespace Postnauka.Pages
{
    class CoursesPage
    {
        private ElementExtension _materialNumber = new ElementExtension(By.ClassName("cc_preview__num"));

        public string GetNumberOfCourseMaterials() => _materialNumber.GetCollection().Count.ToString();
    }
}
