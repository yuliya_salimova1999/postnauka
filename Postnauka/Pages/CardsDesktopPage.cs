﻿using OpenQA.Selenium;
using Postnauka.Extensions;

namespace Postnauka.Pages
{
    class CardsDesktopPage
    {
        private ElementExtension _card = new ElementExtension(By.XPath("//a[contains(@class,'card ')]"));

        public void ClickCard() => _card.Click();

        public string GetNumberOfCourseMaterials() => _card.FindElement(By.XPath("//div[contains(@class,'card-desktop__module-materials')]")).Text.Replace(" материалов", "");
    }
}
