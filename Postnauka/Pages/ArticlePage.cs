﻿using OpenQA.Selenium;
using Postnauka.Extensions;
using System;
using System.Collections.Generic;

namespace Postnauka.Pages
{
    class ArticlePage
    {
        private ElementExtension _articleHeader = new ElementExtension(By.XPath("//div[@class='heading']/h1"));
        private ElementExtension _themeName = new ElementExtension(By.ClassName("init-words-name"));
        private ElementExtension _themeTags = new ElementExtension(By.XPath("//a[contains(@class, 'old-tags-btn')]"));
        private const string _themeTagsLocatorStr = "//a[contains(@class, 'old-tags-btn')]";

        public string GetArticleName() => _articleHeader.GetText();

        public bool IsHeaderNotEqual(string text) => _articleHeader.IsNotEqualWait(text);

        public bool IsThemeEqual(string theme)
        {
            List<ElementExtension> elements = new List<ElementExtension>();
            for (int i = 1; i <= _themeTags.GetCollection().Count; i++)
            {
                elements.Add(new ElementExtension(By.XPath(_themeTagsLocatorStr + $"[{i}]")));
                if (elements[i-1].GetText().Equals(theme, StringComparison.OrdinalIgnoreCase)) return true;
            }
            if (_themeName.GetText().Equals(theme, StringComparison.OrdinalIgnoreCase)) return true;
            return false;
        }
    }
}
