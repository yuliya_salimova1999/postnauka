﻿using OpenQA.Selenium;
using Postnauka.Extensions;
using System;
using System.Collections.Generic;

namespace Postnauka.Pages
{
    class GameTestPage
    {
        private ElementExtension _startTest = new ElementExtension(By.ClassName("start-test"));
        private ElementExtension _nextQuest = new ElementExtension(By.ClassName("next-question"));
        private ElementExtension _answerOption = new ElementExtension(By.XPath("//div[@class='answer ng-star-inserted']"));
        private ElementExtension _numberOfQuestions = new ElementExtension(By.XPath("//span[@class='current-question']/following-sibling::span"));

        public void ClickStartTestButton() => _startTest.Click();

        public void ClickNextQuestionButton() => _nextQuest.Click();

        public string GetNumberOfQuestions() => _numberOfQuestions.GetText().Replace("/", "");

        public string ClickAnswerOptions()
        {
            int numberOfQuestions = 0;
            List<ElementExtension> elements = new List<ElementExtension>();
            for (int i = 1; i <= _answerOption.GetCollection().Count; i++)
            {
                elements.Add(new ElementExtension(By.XPath($"//div[@class='answer ng-star-inserted'][{i}]")));
            }
            ElementExtension element = elements[new Random().Next(0, elements.Count)];
            while (element.IsExists() && _nextQuest.IsExists())
            {
                element.Click();
                _nextQuest.Click();
                numberOfQuestions++;
            }
            return numberOfQuestions.ToString();
        }
    }
}
