﻿using OpenQA.Selenium;
using Postnauka.Extensions;

namespace Postnauka.Pages
{
    class AuthorizationPage
    {
        private ElementExtension _emailField = new ElementExtension(By.Id("email"));
        private ElementExtension _passwordField = new ElementExtension(By.Id("password"));
        private ElementExtension _submitButton = new ElementExtension(By.ClassName("auth__button"));
        private ElementExtension _forgotPasswordLink = new ElementExtension(By.XPath("//a[contains(@class,'uth__link-small')]"));
        private ElementExtension _loginedIcon = new ElementExtension(By.XPath("//a[contains(@class,'m-menu__login_logined')]"));
        private ElementExtension _emailErrorMessage = new ElementExtension(By.XPath("//*[@labelfor='email']//p"));
        private ElementExtension _passwordErrorMessage = new ElementExtension(By.XPath("//*[@labelfor='password']//p"));

        public void EnterEmail(string email) => _emailField.SendKeys(email);

        public void EnterPassword(string password) => _passwordField.SendKeys(password);

        public void ClickForgotPasswordLink() => _forgotPasswordLink.Click();

        public void ClickSubmitButton() => _submitButton.Click();

        public bool IsAuthorizationSuccessful() => _loginedIcon.IsExists();

        public bool IsEmailErrorMessageAppeared() => _emailErrorMessage.IsExists();

        public bool IsPasswordErrorMessageAppeared() => _passwordErrorMessage.IsExists();
    }
}
