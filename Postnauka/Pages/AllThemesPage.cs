﻿using OpenQA.Selenium;
using Postnauka.Extensions;
using System;
using System.Collections.Generic;

namespace Postnauka.Pages
{
    class AllThemesPage
    {
        private ElementExtension _alphabet = new ElementExtension(By.XPath("//li[@class='ng-star-inserted']/a"));
        private ElementExtension _showMoreButton = new ElementExtension(By.XPath("//button[contains(@class, 'theme-show-more')]"));
        private ElementExtension _cards = new ElementExtension(By.XPath("//a[contains(@class, 'card ')]"));
        private const string _alphabetLocatorStr = "//li[@class='ng-star-inserted']/a";
        private const string _cardsLocatorStr = "//a[contains(@class, 'card ')]";

        public string ClickAlphabetLetter()
        {
            List<ElementExtension> elements = new List<ElementExtension>();
            int position = 0;
            for (int j = 0; j < _alphabetLocatorStr.Length; j++)
            {
                if (_alphabetLocatorStr[j] == '/' && _alphabetLocatorStr[j + 1] == 'a')
                {
                    position = j;
                    break;
                }

            }
            for (int i = 1; i <= _alphabet.GetCollection().Count; i++)
            {
                elements.Add(new ElementExtension(By.XPath(_alphabetLocatorStr.Insert(position, $"[{i}]"))));
            }
            position = new Random().Next(0, elements.Count);
            string letter = elements[position].GetText();
            elements[position].Click();
            return letter;
        }

        public bool CheckFirstLetter(string letter)
        {
            while (_showMoreButton.IsExists()) _showMoreButton.Click();
            List<ElementExtension> elements = new List<ElementExtension>();
            for (int i = 1; i <= _cards.GetCollection().Count; i++)
            {
                elements.Add(new ElementExtension(By.XPath(_cardsLocatorStr + $"[{i}]")));
                if (!elements[i-1].FindElement(By.TagName("span")).Text[0].ToString().Equals(letter, StringComparison.OrdinalIgnoreCase)) return false;
            }
            return true;
        }
    }
}
