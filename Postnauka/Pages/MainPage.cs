﻿using OpenQA.Selenium;
using Postnauka.Extensions;
using System;
using System.Collections.Generic;

namespace Postnauka.Pages
{
    class MainPage
    {
        private ElementExtension _logInButton = new ElementExtension(By.XPath("//a[contains(@class, 'm-menu__login')]"));
        private ElementExtension _loginedButton = new ElementExtension(By.XPath("//a[contains(@class,'m-menu__login_logined')]"));
        private ElementExtension _searchButton = new ElementExtension(By.XPath("//div[contains(@class, 'm-menu__search ')]"));
        private ElementExtension _searchField = new ElementExtension(By.XPath("//input[@formcontrolname='searchbox']"));
        private ElementExtension _searchSubmitButton = new ElementExtension(By.XPath("//button[@class='m-menu__btn' and @type='submit']"));
        private ElementExtension _searchResults = new ElementExtension(By.XPath("//a[contains(@class,'card ')]"));
        private ElementExtension _randomizerButton = new ElementExtension(By.XPath("//div[contains(@class,'m-menu__random-post')]"));
        private ElementExtension _themesButton = new ElementExtension(By.XPath("//div[@class='menu-level']/preceding-sibling::a[@href='/themes']"));
        private ElementExtension _allThemesLink = new ElementExtension(By.XPath("//div[@class='theme-item_more']/a"));
        private ElementExtension _themesCollection = new ElementExtension(By.XPath("//a[contains(@class,'themes-menu-item')]"));
        private ElementExtension _animateButton = new ElementExtension(By.XPath("//div[@class='footer__bottom--item']//a[@href='/animate']"));
        private ElementExtension _anyAnimatedElement = new ElementExtension(By.XPath("//div[contains(@class,'animate-special--bg')]"));
        private ElementExtension _theBiggestCard = new ElementExtension(By.XPath("//a[contains(@class,'p-card' )]"));
        private ElementExtension _theBiggestCardTitle = new ElementExtension(By.ClassName("p-card__title"));
        private ElementExtension _coursesButton = new ElementExtension(By.XPath("//div[@class='m-menu__links__link']/a[@href='/courses']"));
        private ElementExtension _gamesButton = new ElementExtension(By.XPath("//div[@class='m-menu__links__link']/a[@href='/tests']"));

        public void ClickLogInButton() => _logInButton.Click();

        public void ClickLoginedButton() => _loginedButton.Click();

        public void ClickSearchButton() => _searchButton.Click();

        public void EnterSearchFieldInput(string input) => _searchField.SendKeys(input);

        public void ClickSearchSubmitButton() => _searchSubmitButton.Click();

        public void ClickRandomizerButton() => _randomizerButton.Click();

        public void ClickAnimateButton() => _animateButton.Click();

        public void ClickCoursesButton() => _coursesButton.Click();

        public void ClickAllThemesLink() => _allThemesLink.Click();

        public void ClickGamesButton() => _gamesButton.Click();

        public void MoveYoThemesButton() => _themesButton.MoveTo();

        public string ClickRandomTheme()
        {
            var element = GetRandomTheme();
            string themeName = element.FindElement(By.ClassName("theme-item-name")).Text;
            element.MoveTo();
            element.Click();
            return themeName;
        }

        public bool IsSearchWorks() => _searchResults.IsExists();

        public bool IsAnimatedButtonWorks() => _anyAnimatedElement.IsExists();

        public string ChooseTheBiggestCard()
        {
            string cardTitle = _theBiggestCardTitle.GetText();
            _theBiggestCard.Click();
            return cardTitle;
        }

        private ElementExtension GetRandomTheme()
        {
            List<ElementExtension> elements = new List<ElementExtension>();
            foreach (IWebElement element in _themesCollection.GetCollection())
            {
                string locator = $"//a[@href='{element.GetAttribute("href").Replace("https://postnauka.ru", "")}']";
                elements.Add(new ElementExtension(By.XPath(locator)));
            }
            return elements[new Random().Next(0, elements.Count)];
        }
    }
}
