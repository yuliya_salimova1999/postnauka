﻿using NUnit.Framework;
using Postnauka.Steps;

namespace Postnauka.Tests
{
    class SearchAnimateRandomizeTest : BaseTest
    {
        private SearchAnimateRandomizeSteps _interactionSteps => new SearchAnimateRandomizeSteps();
        private const string _searchInput = "астрономия";

        [Test]
        public void SearchWorks() => Assert.IsTrue(_interactionSteps.Search(_searchInput));

        [Test]
        public void AnimateWorks() => Assert.IsTrue(_interactionSteps.Animate());

        [Test]
        public void RandomizeWorks() => Assert.IsTrue(_interactionSteps.Randomize());
    }
}
