﻿using NUnit.Framework;
using Postnauka.Steps;

namespace Postnauka.Tests
{
    class AuthorizationTest : BaseTest
    {
        private AuthorizationSteps _authSteps => new AuthorizationSteps();
        private const string _correctEmail = "ya.yulka.san@gmail.com";
        private const string _correctPassword = "8278146";
        private const string _incorrectEmail = "ya.yulka.san@gmail.ru";
        private const string _incorrectPassword = "8278147";

        [Test]
        public void SuccessfulAuthorization() => Assert.IsTrue(_authSteps.SuccessfulAuthorization(_correctEmail, _correctPassword));

        [Test]
        public void IncorrectEmailValidationExists() => Assert.IsTrue(_authSteps.IncorrectEmail(_incorrectEmail, _correctPassword));

        [Test]
        public void IncorrectPasswordValidationExists() => Assert.IsTrue(_authSteps.IncorrectPassword(_correctEmail, _incorrectPassword));
    }
}
