﻿using NUnit.Framework;
using OpenQA.Selenium;
using Postnauka.Extensions;
using System;
using System.Threading;

namespace Postnauka.Tests
{
    abstract class BaseTest
    {
        private const string _mainPageUrl = "https://postnauka.ru/";

        [SetUp]
        public void Setup()
        {
            IWebDriver driver = WebDriver.GetWebDriver(Thread.CurrentThread.ManagedThreadId);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(_mainPageUrl);
        }

        [TearDown]
        public void Dispose()
        {
            WebDriver.GetWebDriver(Thread.CurrentThread.ManagedThreadId).Quit();
            WebDriver.RemoveDriver(Thread.CurrentThread.ManagedThreadId);
        }
    }
}
