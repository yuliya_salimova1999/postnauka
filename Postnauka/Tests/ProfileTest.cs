﻿using NUnit.Framework;
using Postnauka.Steps;

namespace Postnauka.Tests
{
    class ProfileTest : BaseTest
    {
        private ProfileSteps _profileSteps => new ProfileSteps();
        private const string _correctEmail = "ya.yulka.san@gmail.com";
        private const string _correctPassword = "8278146";

        [Test]
        public void ProfileHistoryOptionWorks() => Assert.IsTrue(_profileSteps.CheckHistoryOption(_correctEmail, _correctPassword));
    }
}
