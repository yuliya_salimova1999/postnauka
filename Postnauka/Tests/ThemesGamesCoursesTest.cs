﻿using NUnit.Framework;
using Postnauka.Steps;

namespace Postnauka.Tests
{
    class ThemesGamesCoursesTest : BaseTest
    {
        private ThemesGamesCoursesSteps _steps => new ThemesGamesCoursesSteps();

        [Test]
        public void ChosenThemeEqualsArticleTheme() => Assert.IsTrue(_steps.CompareArticleTheme());

        [Test]
        public void CorrectFirstLetterSort() => Assert.IsTrue(_steps.CompareFirstLetters());

        [Test]
        public void CorrectNumberofCourseMaterials() => Assert.IsTrue(_steps.CompareCourseMaterialsNumber());

        [Test]
        public void CorrectNumberofTestQuestions() => Assert.IsTrue(_steps.CompareGameQuestionsNumber());
    }
}
